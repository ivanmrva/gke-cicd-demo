gcp gitlab ci runner

staging vs production - namespaces vs different clusters?

dns name for endpoints

canary release - change small amounts of production pods
- master + staging + feature branch git workflow
- new environment / namespace pro feature + kubectl proxy
- rollback of staging?
kubectl rollout undo deployment/nginx-deployment --to-revision=2 ?

$ kubectl --namespace=production apply -f k8s/production

config yml for services / replicas

custom pipeline phases (build image, e2e test, push image , etc.)

federated cluster for production

kubectl patch deployment vs set deployment vs edit deployment

cache?

monitoring
- readinesProbe
                           httpGet:
                             path: /healthz
                             port: 8080

logging

hystrix circuits