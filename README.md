# Requirements
Tested with:
* Docker version X
* kubectl 1.5.2
* gcloud version 145.0.0

#

1. Create a new project on Google Cloud Platform (and enable the billing).
   Remember the project ID; it will be referred to later in this codelab as $PROJECT_ID.
2. Create a kubernetes cluster:
   First, choose a Google Cloud Project zone to run your service. 
   gcloud config set compute/zone us-central1-a
   Now, create a cluster via the gcloud command line tool:
   gcloud container clusters create hello-world
3. To create a service account and have your application use it for API
    access, run:

        $ gcloud iam service-accounts create hello-world-account
        $ gcloud iam service-accounts keys create hellow-world-account-key.json --iam-account=hello-world-account@hello-world-project.iam.gserviceaccount.com
        $ export GOOGLE_APPLICATION_CREDENTIALS=hello-world-account-key.json
      

4. assign permissions to service accounts - container.developer + project.edit ?

- gcloud_service_key secure variable
 private_token secure variable


environment urls

kubernetes docker registry secret